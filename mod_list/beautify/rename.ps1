$Path = '.'
$Filter = '*.jar'
$Prefix = '[VANITY]'
$Counter = 1

Get-ChildItem -Path $Path -Filter $Filter -Recurse |
  Rename-Item -NewName {
    $extension = [System.IO.Path]::GetExtension($_.Name)
    '{0}{1}' -f $Prefix, $_.Name, $extension
    $script:Counter++
   }